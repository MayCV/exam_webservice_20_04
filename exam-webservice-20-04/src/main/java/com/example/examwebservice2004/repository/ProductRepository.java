package com.example.examwebservice2004.repository;

import com.example.examwebservice2004.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
