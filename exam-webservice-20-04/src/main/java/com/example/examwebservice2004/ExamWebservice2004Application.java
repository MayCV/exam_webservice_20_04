package com.example.examwebservice2004;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamWebservice2004Application {

    public static void main(String[] args) {
        SpringApplication.run(ExamWebservice2004Application.class, args);
    }

}
